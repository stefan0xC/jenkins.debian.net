#!/bin/bash

set -e

WORKSPACE=/srv/janitor/debian-janitor

export BRZ_PLUGINS_AT=debian@$WORKSPACE/breezy-debian
export PYTHONPATH=$WORKSPACE:$WORKSPACE/breezy:$WORKSPACE/silver-platter:$WORKSPACE/lintian-brush:$WORKSPACE/dulwich:$WORKSPACE/python-debian/lib:$WORKSPACE/debmutate
export SBUILD_CONFIG=/srv/janitor/sbuildrc
export AUTOPKGTEST=$WORKSPACE/autopkgtest-wrapper
python3 -m janitor.pull_worker \
    --base-url=https://janitor.debian.net/api/ \
    --build-command="sbuild -A -v -c unstable-amd64-sbuild" \
    --credentials=/srv/janitor/janitor.creds.json
