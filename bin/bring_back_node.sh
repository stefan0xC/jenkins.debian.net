#!/bin/bash
# vim: set noexpandtab:

# Copyright © 2020 Holger Levsen (holger@layer-acht.org)
# released under the GPLv2
#
# small helper script to bring nodes back online
# FIXME: this script should call the powercycling scripts too

SHORTNAMES=$@
for shortname in $SHORTNAMES ; do
	case $shortname in
		pb2|pb6|pb12|pb16)			i=$(echo $shortname | sed 's#pb##')
							node=profitbricks-build$i-i386.debian.net
							;;
		pb1|pb3|pb5|pb7|pb9|pb10|pb11|pb15)	i=$(echo $shortname | sed 's#pb##')
							node=profitbricks-build$i-amd64.debian.net
							;;
		ct*)					i=$(echo $shortname | sed 's#ct##')
							node=codethink-sled$i-arm64.debian.net
							;;
		*)					echo unsupported, please improve this script
							exit 1
							;;
	esac
	echo "Marking $node as online for jenkins again."
	ssh jenkins.debian.net "sudo -u jenkins -i sed -i 's#$node##' offline_nodes"
done
echo
ssh jenkins.debian.net "sudo -u jenkins -i cat offline_nodes"

